const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const express = require('express')

const event = require('./model')

const app = express()

// Conexion de mongodb
mongoose.connect('mongodb://localhost/recruitment')
mongoose.Promise = global.Promise

// Monitor
app.use(require('express-status-monitor')({
  path: '/status'
}))

// Parser de peticiones POST
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.send('tamo')
})

app.post('/save', (req, res) => {
  console.log(req.body)
  event.create(req.body, (err, doc) => {
    if(err)
      res.send(err)
    res.json(doc)
  })
})

app.listen(8000, function () {
  console.log('Example app listening on port 3000!')
})