const mongoose = require('mongoose');

const Schema = new mongoose.Schema({
  name: String,
  json: String
})

Schema.statics.create = function(data, cb){
  const event = new this({
    name: data.name,
    json: data.json
  })
  event.save(cb)
}

module.exports= mongoose.model('event', Schema)
